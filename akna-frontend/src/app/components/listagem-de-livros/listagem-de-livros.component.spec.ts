import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemDeLivrosComponent } from './listagem-de-livros.component';

describe('ListagemDeLivrosComponent', () => {
  let component: ListagemDeLivrosComponent;
  let fixture: ComponentFixture<ListagemDeLivrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemDeLivrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemDeLivrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
